<?php

use Illuminate\Database\Seeder;

class Setting_namesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('setting_names')->insert([
        'name' => 'minlevel',
      ]);
      DB::table('setting_names')->insert([
        'name' => 'maxlevel',
      ]);
      DB::table('setting_names')->insert([
        'name' => 'alertlevel',
      ]);
      DB::table('setting_names')->insert([
        'name' => 'interval',
      ]);
      DB::table('setting_names')->insert([
        'name' => 'depth',
      ]);
      DB::table('setting_names')->insert([
        'name' => 'chart_time',
      ]);
    }
}
