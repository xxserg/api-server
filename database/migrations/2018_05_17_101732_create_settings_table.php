<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('settings')) {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id');
            $table->integer('setting_names_id');
            $table->integer('value');
            //$table->foreign('device_id')->references('id')->on('devices');
            //$table->foreign('setting_id')->references('id')->on('settings');
            $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
