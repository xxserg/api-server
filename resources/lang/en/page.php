<?php

return [

  'menu' => [
    ['name' => 'Download', 'link' => 'download'],
    ['name' => 'Features',  'link' => 'features'],
    ['name' => 'Device', 'link' => 'device'],
    ['name' => 'Contact', 'link' => 'contact']
  ],
  'head' => "Does my pump work? What is the water level in the well?",
  'download' => "Our app is available on any mobile device! Download now to get started!",
  'features' => [
    'title' => "Pump control system",
    'text' => "The system consists of a ultrasonic level measurement device, a cloud storage service and a mobile application. Installing the device in the well and connecting the pump to it, You can always know the real water level and set the pump operation settings within wide limits. You need a wi-fi network with Internet access to operate the system."
  ],
  'level' => [
    'title' => "Level conrol",
    'text' =>  "You can always know the real water level",
  ],
  'settings' => [
    'title' => "Flexible settings",
    'text' =>  "Setup water levels to switch your pump on and off"
  ],
  'stat' => [
    'title' => "Pump stats",
    'text' =>  "Water level and pump operations reports"
  ],
  'push' => [
    'title' => "Push notifications",
    'text' =>  "Setup alerts for water level changes"
  ],
  'device' => [
    'title' => "Water level sensor",
    'text' => "An ultrasonic sensor is used to measure the water level.
  The device connects to wi-fi network, sends data and receives settings through the cloud.
  After saving the settings, the device can autonomously control the pump."
],
  'external' => [
    'title' => "Controller with external ultrasonic sensor",
    'text' => "Device with external ultrasonic sensor, power cord and pump outlet"
  ],
  'cable' => [
    'title' => "Wired controller",
    'text' => "Controller with conductive level sensors. Using different lenght wires as a sensors.
    Can use up to 4 differt levels for app control."
  ],
  'contact' => [
    'text' => "Pre-order device, or write us for any questions",
    'name' => 'Name',
    'name_validation' => "Please enter your name",
    'email' => "Email Address",
    'email_validation' => "Please enter your email address",
    'phone' => "Phone Number",
    'phone_validation' => "",
    'message' => 'Message',
    'message_validation' => "Please enter a message",
    'send' => 'Send'
  ]

];
