<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <div class="container">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">Pumpcontrol.ru</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">

        @foreach ($menu as $item)
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#{{ $item['link'] }}">{{ $item['name'] }}</a>
        </li>
        @endforeach

        <li class="nav-item">
          <a class="nav-link" href="/ru"><image title="русский" src="/images/ru-16.png"></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/en"><image title="english" src="/images/usa-16.png"></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
