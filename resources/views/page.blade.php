@extends('layouts.app')

@section('content')
<header class="masthead">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-5 my-auto">
          <div class="header-content mx-auto">
            <h1 class="mb-5">{{__('page.head')}}</h1>

          </div>
        </div>
        <div class="col-lg-7 my-auto">
          <div >
            <div>
              <img src="/images/cover2.png" class="img-fluid" alt="">
            </div>
          </div>
        </div>

      </div>
</div>
</header>
<section class="download bg-primary text-center" id="download">
    <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <h2 class="section-heading"></h2>
          <p>{{ __('page.download') }}</p>
          <div class="badges">
            <a class="badge-link" href="https://play.google.com/store/apps/details?id=com.ultrapump">
              <img src="/images/google-play-badge.svg" alt="">
            </a>
            <a class="badge-link" href="#"><img src="/images/app-store-badge.svg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="features" id="features">
    <div class="container">
      <div class="section-heading text-center">
        <h2>{{ __('page.features.title') }}</h2>
        <p class="text-muted">
          {{ __('page.features.text') }}
        </p>
        <hr>
      </div>
      <div class="row">
        <div class="col-lg-4 my-auto">
          <div class="device-container">
            <div class="device-mockup iphone6_plus portrait white">
              <div class="device">
                <div class="screen">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="/images/demo-screen-2.jpg" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8 my-auto">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="icon-eye text-primary"></i>
                  <h3>{{ __('page.level.title') }}</h3>
                  <p class="text-muted">{{ __('page.level.text') }}</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="icon-equalizer text-primary"></i>
                  <h3>{{ __('page.settings.title') }}</h3>
                  <p class="text-muted">{{ __('page.settings.text') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="icon-chart text-primary"></i>
                  <h3>{{ __('page.stat.title') }}</h3>
                  <p class="text-muted">{{ __('page.stat.text') }}</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="icon-bell text-primary"></i>
                  <h3>{{ __('page.push.title') }}</h3>
                  <p class="text-muted">{{ __('page.push.text') }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="device">
    <div class="container">
        <div class="row">
          <div class="col-lg-7 ">
              <h2 class="mb-5">{{ __('page.device.title') }}</h2>
              <p class="text-muted">
                {{ __('page.device.text') }}
              </p>
          </div>
          <div class="col-lg-5 ">
            <div class="device-container">
              <div>
                <img src="/images/sensor4.png" class="img-fluid" alt="">
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>

  <section id="external">
    <div class="container">
        <div class="row">

          <div class="col-lg-5 ">
            <div class="device-container">
              <div>
                <img src="/images/external2.png" class="img-fluid" alt="">
              </div>
            </div>
          </div>

          <div class="col-lg-7 ">
              <h2 class="mb-5">{{ __('page.external.title') }}</h2>
              <p class="text-muted">
                {{ __('page.external.text') }}
              </p>
          </div>
        </div>
    </div>
  </section>
  <section id="external">
    <div class="container">
        <div class="row">
          <div class="col-lg-7 ">
              <h2 class="mb-5">{{ __('page.cable.title') }}</h2>
              <p class="text-muted">
                {{ __('page.cable.text') }}
              </p>
          </div>
          <div class="col-lg-5 ">
            <div class="device-container">
              <div>
                <img src="/images/cable4.png" class="img-fluid" alt="">
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>

  <!-- -->
<section class="contact bg-primary" id="contact">
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      <p>{{ __('page.contact.text') }}
        <form method="post" name="sentMessage" id="contactForm" action="contacts.store" novalidate>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <input type="text" class="form-control" placeholder="{{ __('page.contact.name') }}" id="name" required data-validation-required-message="{{ __('page.contact.name_validation') }}">
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <input type="email" class="form-control" placeholder="{{ __('page.contact.email') }}" id="email" required data-validation-required-message="{{ __('page.contact.email_validation') }}">
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group col-xs-12 floating-label-form-group controls">
            <input type="tel" class="form-control" placeholder="{{ __('page.contact.phone') }}" id="phone">
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls">
            <textarea rows="5" class="form-control" placeholder="{{ __('page.contact.message') }}" id="message" required data-validation-required-message="{{ __('page.contact.message_validation') }}"></textarea>
            <p class="help-block text-danger"></p>
          </div>
        </div>
        <br>
        <div id="success"></div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="sendMessageButton">{{ __('page.contact.send') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>
</section>

  <!-- -->
  <section class="contact bg-primary" id="contact2">
      <div class="container">
        <ul class="list-inline list-social">
          <li class="list-inline-item social-facebook">
            <a href="https://www.facebook.com/pumpcontrolru">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item social-google-plus">
            <a href="https://vk.com/pumpcontrol">
              <i class="fa fa-vk"></i>
            </a>
          </li>
        </ul>
      </div>
  </section>
@endsection
