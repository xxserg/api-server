<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="author" content="Sergey Fomin Pumpcontrol.ru">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>"{{ config('app.name', '') }}"</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



    <!-- Plugin CSS -->
    <link rel="stylesheet" href="/css/device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Metrica -->

  </head>

  <body id="page-top">
    @component ('navigation', ['menu' => __('page.menu')])
    @endcomponent

    @yield('content')

    <footer>
      <div class="container">
        <p>&copy; Pumpcontrol.ru 2018 - 2019. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="/privacy.html">Privacy</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Terms</a>
          </li>
          <li class="list-inline-item">
            <a href="/faq.html">FAQ</a>
          </li>
        </ul>
      </div>
    </footer>



  </body>

</html>
