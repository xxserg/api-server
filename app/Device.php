<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{



  protected $fillable = [
      'user_id', 'name', 'device_id', 'token', 'type'
  ];

    /**
     * Get the settings.
     */
    public function settings()
    {
        return $this->hasMany('App\Settings');
    }

    public function getAlerts()
    {
        return $devices = $this->with(
          [
          'settings' => function($query) {
            $query->where('setting_names_id', 3);
          }]
          )->get();
        //return $this->has('settings')->where('setting_names_id', 3)->get();

    }
}
