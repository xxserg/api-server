<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting_names extends Model
{
  public function settings()
  {
      return $this->hasMany('App\Settings');
  }
}
