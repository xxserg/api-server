<?php

namespace App\Admin\Controllers;

use App\Text;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TextController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Text';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Text);

        $grid->column('id', __('Id'));
        $grid->column('alias', __('Alias'));
        $grid->column('title-en', __('Title en'));
        $grid->column('body-en', __('Body en'));
        $grid->column('title-ru', __('Title ru'));
        $grid->column('body-ru', __('Body ru'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Text::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('alias', __('Alias'));
        $show->field('title-en', __('Title en'));
        $show->field('body-en', __('Body en'));
        $show->field('title-ru', __('Title ru'));
        $show->field('body-ru', __('Body ru'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Text);

        $form->text('alias', __('Alias'));
        $form->text('title-en', __('Title en'));
        $form->textarea('body-en', __('Body en'));
        $form->text('title-ru', __('Title ru'));
        $form->textarea('body-ru', __('Body ru'));

        return $form;
    }
}
