<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use InfluxDB;
use App\Notifications\AlertLevel;

class Settings extends Model
{

  protected $fillable = [
      'device_id', 'setting_names_id', 'value',
  ];

  public function setting_names()
  {
      return $this->belongsTo('App\Setting_names');
  }

  public function device()
  {
      return $this->belongsTo('App\Device');
  }

  public static function send_mqtt($topic, $value)
  {
    $c = new \Mosquitto\Client;
    $c->setCredentials(env('MQTT_USER'), env('MQTT_PASS'));
    $c->connect(env('MQTT_HOST'));
    //$c->loopForever();
    //echo "Finished\n";
    	$c->loop();
    	$mid = $c->publish($topic, $value, 1, 0);
    	//echo "Sent message ID: {$mid} $topic, $value\n";
    	$c->loop();

    $c->disconnect();
    unset($c);
    return true;
  }

  public static function alert()
  {
    $alerts = Settings::where('setting_names_id', 3)->with('device')->get();
    foreach ($alerts as $alert) {
        //print_r($alert);
        $device_id = $alert->toArray()["device"]["device_id"];
        $level = '';

        $result = InfluxDB::query(
          "select time, value from mqtt_consumer where topic = '".$device_id."/level' order by time desc limit 1"
        );
        if ($points = $result->getPoints()) {
          $level = $points[0]['value'];
        }

        $result = InfluxDB::query(
          "select time, value from mqtt_consumer where topic = '".$device_id."/connected'  order by time desc limit 1"
        );
        if ($points = $result->getPoints()) {
          $connected = $points[0]['value'];
        }
        if ($connected && $level && $level < $alert->toArray()['value']) {
            echo $device_id." Level: ".$level." alert: ".$alert->toArray()['value']."\n";
            $user = User::find($alert->toArray()["device"]["user_id"]);
            if ($user->player_id) {
                $user->notify(new AlertLevel($alert->toArray()["device"], $level));
            }
        }
    }
    return;
  }

}
