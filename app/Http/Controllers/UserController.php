<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('home');
    }

    public function update(Request $request)
    {
      $user = Auth::user();
      $this->validate(request(), [
        'pushToken' => 'required',
        'userId' => 'required',
      ]);

      $user->push_token = $request->pushToken;
      $user->player_id = $request->userId;
      $user->save();

      return $user;
    }
}
