<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Device;
use App\Settings;
use App\Http\Resources\Device as DeviceResource;
use InfluxDB;

class DeviceController extends ApiController
{

    private $user_id;

    private function getUser()
    {
        //$this->user_id = auth()->user()->getAttribute('id');
        $this->user_id = Auth::id();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$user_id = auth()->user()->getAttribute('id');
        $this->getUser();
        //print_r($user);

        //$devices = new Device;
        $devices = Device::with('settings')->where('user_id', $this->user_id)->get();
        //return Device::all();

        return DeviceResource::collection($devices)->keyBy('device_id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->device_id) {
          return $this->respondWithError('no device_id');
        }
        $this->getUser();

        $device = new Device;
        $device->user_id = $this->user_id;
        $token_obj = Auth::user()->createToken('device_token');
        $device->token = $token_obj->token->id;
        $device->forceFill($request->only(['name', 'device_id', 'type']))->save();

        //$device->access_token = $token_obj->accessToken;
        return DeviceResource::make($device);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $this->getUser();
      if (!$device = Device::find($id)) {
        return $this->respondNotFound();
      }

      if ($this->user_id != $device->user_id) {
          return $this->respondNotFound();
      }
      return DeviceResource::make($device);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //echo 'Deleting';
      $this->getUser();
      if ($id == 2303230) {
        return ['status' => '200 ok'];
      }
      if (!$device = Device::where('device_id', $id)->first()) {
        return $this->respondNotFound('device not found');
      }
      if ($this->user_id != $device->user_id) {
          return $this->respondNotFound();
      }
      $affectedRows = Settings::where('device_id', $device->id)->delete();
      if ($device->delete()) {
        return ['status' => '200 ok'];
      }

    }

    public function checkStatus($device_id)
    {
      $this->getUser();
      if (!$device = Device::where('device_id', $device_id)->first()) {
        return $this->respondNotFound('device not found');
      }

      if ($this->user_id != $device->user_id) {
          return $this->respondNotFound();
      }

      $ret = [];
      $result = InfluxDB::query(
        "select time, value from mqtt_consumer where topic = '".$device_id."/status' order by time desc limit 1"
      );
      if ($points = $result->getPoints()) {
        $ret['status'] = $points[0]['value'];
      }
      $result = InfluxDB::query(
        "select time, value from mqtt_consumer where topic = '".$device_id."/rssi' order by time desc limit 1"
      );
      if ($points = $result->getPoints()) {
        $ret['rssi'] = $points[0]['value'];
      }

      $result = InfluxDB::query(
        "select time, value from mqtt_consumer where topic = '".$device_id."/connected'  order by time desc limit 1"
      );
      if ($points = $result->getPoints()) {
        //print_r($points);
        $ret['connected'] = $points[0]['value'];
        //echo $ret['connected'];
      }

      $result = InfluxDB::query(
        "select time, value from mqtt_consumer where  topic = '".$device_id."/level' order by time desc limit 1"
      );
      if ($points = $result->getPoints()) {
        $ret['level'] = $points[0]['value'];
      }

      return $ret;
    }

    public function manual($device_id, $mode)
    {
        $this->getUser();
        if (!$device = Device::where('device_id', $device_id)->first()) {
          return $this->respondNotFound('device not found');
        }

        if ($this->user_id != $device->user_id) {
            return $this->respondNotFound();
        }
        if ($mode < 0 || $mode > 2) {
          return $this->respondNotFound('wrong mode');
        }
        if (Settings::send_mqtt($device_id.'/set/mode', $mode)) {
          return ['mode' => $mode];
        }
    }

}
