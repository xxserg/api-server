<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ApiController extends Controller
{
    /**
     * @var integer
     */
    protected $statusCode = 200;

    /**
     * Get HTTP status code.
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set HTTP status code.
     *
     * @param mixed $statusCode
     * @return self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Respond with 'not found' HTTP response error.
     *
     * @param  string $message
     * @return mixed
     */
    public function respondNotFound($message = null)
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)
                    ->respondWithError($message);
    }

    /**
     * Respond with 'internal server error' HTTP response error.
     *
     * @param  string $message
     * @return mixed
     */
    public function respondInternalError($message = null)
    {
        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                    ->respondWithError($message);
    }

    /**
     * Respond with 'unprocessable entity' HTTP response error.
     *
     * @param  string $message
     * @return mixed
     */
    public function respondInvalid($message = null)
    {
        return $this->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
                    ->respondWithError($message);
    }

    /**
     * Respond with an error.
     *
     * @param  string $message
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message'     => $message ?: Response::$statusTexts[$this->getStatusCode()],
                'status_code' => $this->getStatusCode(),
            ]
        ]);
    }

    /**
     * Respond to request.
     *
     * @param  mixed  $data
     * @param  array  $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }
}
