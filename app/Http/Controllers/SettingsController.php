<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Settings;
use App\Setting_names;

class SettingsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     //TODO: move to model
    public function store(Request $request)
    {
      if (!$request->device_id) {
        return $this->respondWithError('no device_id');
      }

      if (!$device = Device::where('device_id', $request->device_id)->first()) {
        return $this->respondNotFound('device not found');
      }

      if ($request->name) {
        $device->name = $request->name;
        $device->save();
      }
      //return $device;
      //print_r($request->all());
      foreach ($request->all() as $key => $value) {
          if ($setting_names = Setting_names::where('name', $key)->first()) {
              $setting[$key] = Settings::updateOrCreate(
                [
                  'device_id' => $device->id,
                  'setting_names_id' => $setting_names->id
                ],
                [
                  'value' => $value
                ]
              );
              // Convert ms to sec
              if ($key == 'interval') {
                $value = $value * 1000;
              }
              Settings::send_mqtt($device->device_id.'/set/'.$key, $value);
          }
      }

      return $setting;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
