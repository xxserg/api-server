<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contact;
use Mail;

class ContactsController extends Controller
{
   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function contacts()
   {
       //return view('contacts');
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function contactsPost(Request $request)
   {
       $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
        ]);

       Contact::create($request->all());

       Mail::send('email',
              array(
                  'name' => $request->get('name'),
                  'email' => $request->get('email'),
                  'user_message' => $request->get('message')
              ), function($message)
          {
              $message->from('xxserg@gmail.com');
              $message->to('xxserg@gmail.com', 'Admin')->subject('Website Contact Form');
          });

       //return back()->with('success', 'Thanks for contacting us!');
   }
}
