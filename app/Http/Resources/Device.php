<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Device extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      /*
      $settings = Settings::collection($this->settings);
      print_r($settings);
      for ($i = 0; $i < sizeof($settings); $i++) {
          $settings[$settings[$i]['name']] = $settings[$i]['value'];
          unset($settings[$i]);
      }
      */
      return  [
         //'id' => $this->id,
         'name' => $this->name,
         'type' => $this->type,
         'device_id' => $this->device_id,
         'settings' => Settings::collection($this->settings),
         //'token' => $this->token,
         //'access_token' => $this->access_token,
         //'created_at' => $this->created_at,
         //'updated_at' => $this->updated_at,
     ];
    }
}
