<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Settings extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
           'name' => Setting_names::make($this->setting_names)->name,
           //'name' => $this->setting_name_id,
           //'device_id' => $this->device_id,
           //'setting_name_id' => Settings::collection($this->settings),
           'value' => $this->value
       ];
    }
}
