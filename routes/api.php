<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function() {
  Route::resource('device', 'DeviceController');
  Route::resource('settings', 'SettingsController');
});

Route::post('register', 'Auth\RegisterController@register');

Route::middleware('auth:api')->get('/device/status/{device_id}', 'DeviceController@checkStatus');
Route::middleware('auth:api')->get('/device/manual/{device_id}/{mode}', 'DeviceController@manual');

Route::middleware('auth:api')->post('/pushtoken', 'UserController@update');

Route::get('alerts', 'DeviceController@alerts');


Route::post('mauth', function () {
    return 'ok post';
});

Route::post('msuperuser', function () {
    return 'ok';
});

Route::post('macl', function () {
    return 'ok';
});
